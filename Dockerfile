FROM java:openjdk-7-jre
MAINTAINER Gordon Knoppe <gknoppe@magento.com>

ENV SOLR_USER solr
ENV SOLR_UID 8983

RUN groupadd -r -g $SOLR_UID $SOLR_USER && \
  useradd -r -u $SOLR_UID -g $SOLR_USER $SOLR_USER

RUN mkdir -p /opt/solr && \
  wget -nv http://archive.apache.org/dist/lucene/solr/3.6.2/apache-solr-3.6.2.tgz -O /opt/solr.tgz

RUN tar -C /opt/solr --extract --file /opt/solr.tgz --strip-components=1 && \
  rm /opt/solr.tgz*

RUN rm -rf /opt/solr/example/solr/conf

ADD ./conf /opt/solr/example/solr/conf

EXPOSE 8983
WORKDIR /opt/solr/example

ADD ./run-solr /opt/solr/example/run-solr

RUN chmod 755  /opt/solr/example/run-solr

CMD ["/opt/solr/example/run-solr"]
